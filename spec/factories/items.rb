# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :item, :class => 'Items' do
    item_log_id 1
    picture "MyText"
    category_id 1
    name "MyString"
    price "9.99"
  end
end
