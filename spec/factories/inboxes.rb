# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :inbox do
    user_id 1
    recipient_id 1
    message "MyText"
  end
end
