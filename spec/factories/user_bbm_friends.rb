# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user_bbm_friend, :class => 'UserBbmFriends' do
    user_id 1
    bbm_friend "MyString"
  end
end
