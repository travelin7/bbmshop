# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :item_log, :class => 'ItemLogs' do
    user_id 1
    status "MyString"
  end
end
