# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user, :class => 'Users' do
    full_name "MyString"
    profile_picture "MyString"
    status ""
    contact "MyText"
    country_id 1
  end
end
