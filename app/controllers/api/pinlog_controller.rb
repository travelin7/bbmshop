class Api::PinlogController < ApplicationController
  
  respond_to :json
  
  def index
    
    if params[:user_id].present?
      pin = Pinlog.find_by_user_id(params[:user_id])
    
      if pin
        render :json => { :status => response.status, :message => [], :pins => pin }
      else
        render :json => { :status => response.status, :message => ["not_found"] }
      end
    end
    
  end
  
  def create
    pin = Pinlog.new(params[:pinlog])
    
    if pin.save
      render :json => { :status => response.status, :message => [], :pins => pin }
    else
      render :json => { :status => response.status, :message => [pin.errors] }
    end
  end
end
