class Api::UploadController < ApplicationController
  
  respond_to :json
  
  def create
   
      user_avatar = Avatar.new(params[:avatar])
     
      if user_avatar.save
        render :json => { :status => response.status, :message => [], :avatars => user_avatar }
      else
        render :json => { :status => response.status, :message => [user_avatar.errors] }
      end
  end
end
