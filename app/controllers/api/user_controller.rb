class Api::UserController < ApplicationController
  respond_to :json
  
  # index
  def index
    # if bb pin exist
    if params[:bb_pin].present?
      users  = Users.find_by_bb_pin(params[:bb_pin])
      avatar = Avatar.find_by_user_id(users.id)
      
      user_array = Hash.new
      user_array['bb_pin']     = users.bb_pin
      user_array['contact']    = users.contact
      user_array['country_id'] = users.country_id
      user_array['created_at'] = users.created_at
      user_array['full_name']  = users.full_name
      user_array['id']         = users.id
      user_array['ppid']       = users.ppid
      user_array['status']     = users.status
      user_array['updated_at'] = users.updated_at
      user_array['avatar']     = avatar.image.url(:download => false)
      
      if users
        # return the record
        render :json => { :status => response.status, :message => [], :user => user_array }
      else
        render :json => { :status => response.status, :message => [users.error] } 
      end
    else
      render :json => { :status => response.status, :message => ["no_bb_pin"] }
    end
  end
  
  def show
    
  end
  
  # create new user
  def create
    @users = Users.new(params[:users])
    
    if @users.save
      render :json => { :status => response.status, :message => [], :user => @users }
    else
      render :json => { :status => response.status, :message => [@users.errors] }
    end
  end

  def update
  end
  
  def destroy
  end
end
