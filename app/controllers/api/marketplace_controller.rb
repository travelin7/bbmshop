class Api::MarketplaceController < ApplicationController
  respond_to :json
  
  def index
    
    # check if this is 1st level of friend
    if params[:friend_level] == "1"
      # get friend level 1
      pin_friend = UserBbmFriends.find_all_by_user_id_and_status(params[:user_id], 1)
      
      # setup the array for json
      marketplace_array = Array.new
      
      pin_friend.each do |pin|
        # I got my friend's pin now
        friend_id_array = Users.find_all_by_bb_pin(pin.bbm_friend)
        
        friend_id_array.each do |friend|
          # now it's time to get the products
          marketplace = Items.find_by_user_id(friend.id)
          
          if marketplace
            marketplace_array.push(marketplace)
          end
          
        end
      end
      
      render :json => { :status => response.status, :message => [], :items => marketplace_array }
    end
    
    # check if this is 2nd level of friend
    if params[:friend_level] == "2"
      sql_query = "select `ppid` from `user_bbm_friends` where `id` in (select `id` from `users` where `ppid` in (select `ppid` from `user_bbm_friends` where `user_id` = #{ params[:user_id] })) and `ppid` not in (select `ppid` from `user_bbm_friends` where `user_id` = #{params[:user_id]}) group by `ppid`"
      pin_friend = UserBbmFriends.find_by_sql(sql_query)
      
      # setup the array for json
      marketplace_array = Array.new
      
      pin_friend.each do |pin|
        # I got my friend's pin now
        friend_id_array = Users.find_all_by_bb_pin(pin.bbm_friend)
        
        friend_id_array.each do |friend|
          # now it's time to get the products
          marketplace = Items.find_by_user_id(friend.id)
          
          if marketplace
            marketplace_array.push(marketplace)
          end
          
        end
      end
      
      render :json => { :status => response.status, :message => [], :items => marketplace_array }
    end
    
    # check if this is a recommendation item
    if params[:friend_level] == "3"
      
    end
    
  end
  
  # Publish new items to the marketplace
  # @param
  def create
    user_item = Items.new(params[:items])
    
    if user_item.save
       render :json => { :status => response.status, :message => [], :user => user_item }
    else
       render :json => { :status => response.status, :message => [user_item.errors] }
    end
  end
end
