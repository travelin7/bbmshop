class Inbox < ActiveRecord::Base
  attr_accessible :message, :recipient_id, :user_id
  
  belongs_to :user
end
