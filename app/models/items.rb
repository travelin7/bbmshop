class Items < ActiveRecord::Base
  attr_accessible :category_id, :item_log_id, :name, :price, :description, :flag, :user_id, :slug
  
  belongs_to :user
  
  before_create :create_slug
  
  validates :category_id, :presence => true
  validates :name, :presence => true, :length => { :minimum => 5 }
  validates :price, :presence => true, :numericality => true
  validates :description, :presence => true
  validates :user_id, :presence => true
  
  private
  def create_slug
    downcased_string = self.name.downcase
    self.slug = downcased_string.gsub(/ /, '-')
  end
  
end
