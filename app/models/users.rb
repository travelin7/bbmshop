class Users < ActiveRecord::Base
  attr_accessible :contact, :country_id, :full_name, :status, :bb_pin, :ppid
  
  has_many :items
  has_one :avatar
  
  validates :bb_pin,
    :presence => true,
    :length => { :maximum => 9 },
    :uniqueness => true
    
  validates :ppid,
    :presence => true,
    :uniqueness => true
end
