class UserBbmFriends < ActiveRecord::Base
  attr_accessible :bbm_friend, :user_id, :status
  
  belongs_to :users
  
  validates :bbm_friend, :presence => true
  validates :user_id, :presence => true
  validates :status, :presence => true
end
