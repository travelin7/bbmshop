class Avatar < ActiveRecord::Base
  attr_accessible :user_id, :image
  
  belongs_to :users
  
  validates :image, :attachment_presence => true
  
  has_attached_file :image, :styles => { :thumb => "70*70", :small => "250*100>", :medium => "720*720>"}, 
                    :storage => :dropbox,
                    :dropbox_credentials => Rails.root.join("config/dropbox.yml"),
                    :dropbox_options => {environment: ENV["RACK_ENV"]},
                    :path => ":style/:id_:filename"

end
