set :application, "BBM Shop"
set :repository,  "https://travelin7@bitbucket.org/travelin7/bbmshop.git"

set :scm, :git # You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

set :deploy_to, "/home/asep/angelhack"

role :web, "travees.com"                          # Your HTTP server, Apache/etc
role :app, "travees.com"                          # This may be the same as your `Web` server
role :db,  "travees.com", :primary => true # This is where Rails migrations will run

set :user, "asep"

default_run_options[:pty] = true

# if you want to clean up old releases on each deploy uncomment this:
#after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
namespace :deploy do
   task :start do ; end
   task :stop do ; end
   task :restart, :roles => :app, :except => { :no_release => true } do
     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
   end
end