AngelHack::Application.routes.draw do
  
  namespace :admin do
    resources :categories
  end


  get "testing/index"

  namespace :api do
    match "pinlog(/:user_id)" => "pinlog#index", :via => :get
    match "pinlog/create" => "pinlog#create", :via => :post
    
    match "upload" => "upload#create", :via => :post
    
    match "user/create" => "user#create", :via => :post 
    match "user/update" => "user#update", :via => :post
    match "user/destroy" => "user#destroy", :via => :delete
    match "user/show" => "user#show", :via => :get
    match "user(/:bb_pin)" => "user#index", :via => :get
    
    match "marketplace(/:friend_level)(/:user_id)" => "marketplace#index", :via => :get
    match "items/create" => "marketplace#create", :via => :post
  end

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'testing#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
