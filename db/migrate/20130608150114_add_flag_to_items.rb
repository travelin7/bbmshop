class AddFlagToItems < ActiveRecord::Migration
  def change
    add_column :items, :flag, :integer, :limit => 3
  end
end
