class AddPpidToUserBbmFriends < ActiveRecord::Migration
  def change
    add_column :user_bbm_friends, :ppid, :string, :length => { :minimum => 255 }
  end
end
