class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.integer :item_log_id
      t.text :picture
      t.integer :category_id
      t.string :name
      t.decimal :price

      t.timestamps
    end
  end
end
