class AddPinToUsers < ActiveRecord::Migration
  def change
    add_column :users, :bb_pin, :string, :null => false, :limit => 9
  end
end
