class ChangeProfilePictureInUsers < ActiveRecord::Migration
  def up
    change_column :users, :profile_picture, :text
  end

  def down
    change_column :users, :profile_picture, :string
  end
end
