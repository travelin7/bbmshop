class CreateUserBbmFriends < ActiveRecord::Migration
  def change
    create_table :user_bbm_friends do |t|
      t.integer :user_id
      t.string :bbm_friend

      t.timestamps
    end
  end
end
