class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :full_name
      t.string :profile_picture
      t.string :status
      t.text :contact
      t.integer :country_id

      t.timestamps
    end
  end
end
