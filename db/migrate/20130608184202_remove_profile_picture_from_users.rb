class RemoveProfilePictureFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :profile_picture
  end

  def down
    add_column :users, :profile_picture, :text
  end
end
