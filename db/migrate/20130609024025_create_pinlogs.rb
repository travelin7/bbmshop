class CreatePinlogs < ActiveRecord::Migration
  def change
    create_table :pinlogs do |t|
      t.integer :item_id
      t.integer :user_id
      t.integer :receiver_id

      t.timestamps
    end
  end
end
