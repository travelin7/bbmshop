class CreateInboxes < ActiveRecord::Migration
  def change
    create_table :inboxes do |t|
      t.integer :user_id
      t.integer :recipient_id
      t.text :message

      t.timestamps
    end
  end
end
