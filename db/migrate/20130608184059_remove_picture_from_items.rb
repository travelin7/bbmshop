class RemovePictureFromItems < ActiveRecord::Migration
  def up
    remove_column :items, :picture
  end

  def down
    add_column :items, :picture, :text
  end
end
