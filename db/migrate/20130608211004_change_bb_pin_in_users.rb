class ChangeBbPinInUsers < ActiveRecord::Migration
  def up
    change_column :users, :bb_pin, :string, :length => { :minimum => 255 }
  end

  def down
    change_column :users, :bb_pin, :string, :length => { :minimum => 9 }
  end
end
