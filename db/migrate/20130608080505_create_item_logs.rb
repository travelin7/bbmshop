class CreateItemLogs < ActiveRecord::Migration
  def change
    create_table :item_logs do |t|
      t.integer :user_id
      t.string :status

      t.timestamps
    end
  end
end
