class AddPpidToUsers < ActiveRecord::Migration
  def change
    add_column :users, :ppid, :string, :length => { :minimum => 255 }
  end
end
