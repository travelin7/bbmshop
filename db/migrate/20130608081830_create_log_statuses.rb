class CreateLogStatuses < ActiveRecord::Migration
  def change
    create_table :log_statuses do |t|
      t.integer :user_id
      t.string :status

      t.timestamps
    end
  end
end
